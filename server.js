var http = require("http");
const app = require("./api/app.js");

//create a server object
var server = http.createServer(app);

var port = process.env.PORT || 8080;

server.listen(port);

console.log("RESTful API server started on: " + port);
