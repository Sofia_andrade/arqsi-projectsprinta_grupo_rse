import { Mapper } from "../core/infra/Mapper";

import { Document, Model } from "mongoose";
import { IDriverPersistence } from "../dataschema/IDriverPersistence";

import IDriverDTO from "../dto/IDriverDTO";
import { Driver } from "../domain/driver";

import { UniqueEntityID } from "../core/domain/UniqueEntityID";

export class DriverMap extends Mapper<Driver> {
  public static toDTO(driver: Driver): IDriverDTO {
    return {
      id: driver.id.toString(),
      name: driver.name
    } as IDriverDTO;
  }

  public static toDomain(
    driver: any | Model<IDriverPersistence & Document>
  ): Driver {
    const driverOrError = Driver.create(
      driver,
      new UniqueEntityID(driver.domainId)
    );

    driverOrError.isFailure ? console.log(driverOrError.error) : "";

    return driverOrError.isSuccess ? driverOrError.getValue() : null;
  }

  public static toPersistence(driver: Driver): any {
    return {
      domainId: driver.id.toString(),
      name: driver.name
    };
  }
}
