import { Mapper } from "../core/infra/Mapper";

import { Document, Model } from "mongoose";
import { IVehiclePersistence } from "../dataschema/IVehiclePersistence";

import IVehicleDTO from "../dto/IVehicleDTO";
import { Vehicle } from "../domain/vehicle";

import { UniqueEntityID } from "../core/domain/UniqueEntityID";

export class VehicleMap extends Mapper<Vehicle> {
  public static toDTO(vehicle: Vehicle): IVehicleDTO {
    return {
      id: vehicle.id.toString(),
      name: vehicle.name
    } as IVehicleDTO;
  }

  public static toDomain(vehicle: any | Model<IVehiclePersistence & Document>): Vehicle {
    const vehicleOrError = Vehicle.create(vehicle, new UniqueEntityID(vehicle.domainId));

    vehicleOrError.isFailure ? console.log(vehicleOrError.error) : "";

    return vehicleOrError.isSuccess ? vehicleOrError.getValue() : null;
  }

  public static toPersistence(vehicle: Vehicle): any {
    return {
      domainId: vehicle.id.toString(),
      name: vehicle.name
    };
  }
}
