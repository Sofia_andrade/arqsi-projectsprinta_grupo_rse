import { Mapper } from "../core/infra/Mapper";

import { Document, Model } from "mongoose";
import { INodePersistence } from "../dataschema/INodePersistence";

import INodeDTO from "../dto/INodeDTO";
import { Node } from "../domain/node";

import { UniqueEntityID } from "../core/domain/UniqueEntityID";

export class NodeMap extends Mapper<Node> {
  public static toDTO(node: Node): INodeDTO {
    return {
      id: node.id.toString(),
      name: node.name
    } as INodeDTO;
  }

  public static toDomain(node: any | Model<INodePersistence & Document>): Node {
    const nodeOrError = Node.create(node, new UniqueEntityID(node.domainId));

    nodeOrError.isFailure ? console.log(nodeOrError.error) : "";

    return nodeOrError.isSuccess ? nodeOrError.getValue() : null;
  }

  public static toPersistence(node: Node): any {
    return {
      domainId: node.id.toString(),
      name: node.name
    };
  }
}
