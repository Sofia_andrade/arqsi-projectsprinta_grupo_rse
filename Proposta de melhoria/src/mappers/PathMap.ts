import { Mapper } from "../core/infra/Mapper";

import { Document, Model } from "mongoose";
import { IPathPersistence } from "../dataschema/IPathPersistence";

import {SinglePath} from "../domain/singlepath";
import { Path } from "../domain/path";
import IPathDTO from "../dto/IPathDTO";

import { UniqueEntityID } from "../core/domain/UniqueEntityID";

export class PathMap extends Mapper<Path> {
  public static toDTO(path: Path): IPathDTO {
    return {
      id: path.id.toString(),
      path: SinglePath,
      orientation: path.orientation
    } as IPathDTO;
  }

  public static toDomain(path: any | Model<IPathPersistence & Document>): Path {
    const pathOrError = Path.create(
      path, 
      new UniqueEntityID(path.linepathkey));

    pathOrError.isFailure ? console.log(pathOrError.error) : "";

    return pathOrError.isSuccess ? pathOrError.getValue() : null;
  }

  public static toPersistence(path: Path): any {
    return {
        linepathkey: path.id.toString(),
        path: path.path,
        orientation: path.orientation
    };
  }
}