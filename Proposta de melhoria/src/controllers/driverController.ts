import { Request, Response, NextFunction } from "express";
import { Inject } from "typedi";
import config from "../../config";

import IDriverController from "./IControllers/IDriverController";
import IDriverService from "../services/IServices/IDriverService";
import IDriverDTO from "../dto/IDriverDTO";

import { Result } from "../core/logic/Result";

export default class DriverController
  implements IDriverController /* TODO: extends ../core/infra/BaseController */ {
  constructor(
    @Inject(config.services.driver.name) private DriverServiceInstance: IDriverService
  ) {}

  public async createDriver(req: Request, res: Response, next: NextFunction) {
    try {
      const driverOrError = (await this.DriverServiceInstance.createDriver(
        req.body as IDriverDTO
      )) as Result<IDriverDTO>;

      if (driverOrError.isFailure) {
        return res.status(402).send();
      }

      const driverDTO = driverOrError.getValue();
      return res.json(driverDTO).status(201);
    } catch (e) {
      return next(e);
    }
  }

  public async updateDriver(req: Request, res: Response, next: NextFunction) {
    try {
      const driverOrError = (await this.DriverServiceInstance.updateDriver(
        req.body as IDriverDTO
      )) as Result<IDriverDTO>;

      if (driverOrError.isFailure) {
        return res.status(404).send();
      }

      const driverDTO = driverOrError.getValue();
      return res.status(201).json(driverDTO);
    } catch (e) {
      return next(e);
    }
  }
}
