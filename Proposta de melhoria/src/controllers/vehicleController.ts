import { Request, Response, NextFunction } from "express";
import { Inject } from "typedi";
import config from "../../config";

import IVehicleController from "./IControllers/IVehicleController";
import IVehicleService from "../services/IServices/IVehicleService";
import IVehicleDTO from "../dto/IVehicleDTO";

import { Result } from "../core/logic/Result";

export default class VehicleController
  implements IVehicleController /* TODO: extends ../core/infra/BaseController */ {
  constructor(
    @Inject(config.services.vehicle.name) private VehicleServiceInstance: IVehicleService
  ) {}

  public async createVehicle(req: Request, res: Response, next: NextFunction) {
    try {
      const vehicleOrError = (await this.VehicleServiceInstance.createVehicle(
        req.body as IVehicleDTO
      )) as Result<IVehicleDTO>;

      if (vehicleOrError.isFailure) {
        return res.status(402).send();
      }

      const vehicleDTO = vehicleOrError.getValue();
      return res.json(vehicleDTO).status(201);
    } catch (e) {
      return next(e);
    }
  }

  public async updateVehicle(req: Request, res: Response, next: NextFunction) {
    try {
      const vehicleOrError = (await this.VehicleServiceInstance.updateVehicle(
        req.body as IVehicleDTO
      )) as Result<IVehicleDTO>;

      if (vehicleOrError.isFailure) {
        return res.status(404).send();
      }

      const vehicleDTO = vehicleOrError.getValue();
      return res.status(201).json(vehicleDTO);
    } catch (e) {
      return next(e);
    }
  }
}
