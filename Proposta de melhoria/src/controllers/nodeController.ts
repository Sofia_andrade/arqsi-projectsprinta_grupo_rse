import { Request, Response, NextFunction } from "express";
import { Inject } from "typedi";
import config from "../../config";

import INodeController from "./IControllers/INodeController";
import INodeService from "../services/IServices/INodeService";
import INodeDTO from "../dto/INodeDTO";

import { Result } from "../core/logic/Result";

export default class NodeController
  implements INodeController /* TODO: extends ../core/infra/BaseController */ {
  constructor(
    @Inject(config.services.node.name) private NodeServiceInstance: INodeService
  ) {}

  public async createNode(req: Request, res: Response, next: NextFunction) {
    try {
      const nodeOrError = (await this.NodeServiceInstance.createNode(
        req.body as INodeDTO
      )) as Result<INodeDTO>;

      if (nodeOrError.isFailure) {
        return res.status(402).send();
      }

      const nodeDTO = nodeOrError.getValue();
      return res.json(nodeDTO).status(201);
    } catch (e) {
      return next(e);
    }
  }
  
  public async updateNode(req: Request, res: Response, next: NextFunction) {
    try {
      const nodeOrError = (await this.NodeServiceInstance.updateNode(
        req.body as INodeDTO
      )) as Result<INodeDTO>;

      if (nodeOrError.isFailure) {
        return res.status(404).send();
      }

      const nodeDTO = nodeOrError.getValue();
      return res.status(201).json(nodeDTO);
    } catch (e) {
      return next(e);
    }
  }

  public async getNode(req: Request, res: Response, next: NextFunction) {
    try {
      const nodeOrError = (await this.NodeServiceInstance.getAllNodes(
        req.body as INodeDTO
      )) as Result<INodeDTO>;

      if (nodeOrError.isFailure) {
        return res.status(402).send();
      }

      const nodeDTO = nodeOrError.getValue();
      return res.json(nodeDTO).status(201);
    } catch (e) {
      return next(e);
    }
  }

  public async getNodeById(req: Request, res: Response, next: NextFunction) {
    try {
      const nodeOrError = (await this.NodeServiceInstance.getNodeById(
        req.body as INodeDTO
      )) as Result<INodeDTO>;

      if (nodeOrError.isFailure) {
        return res.status(402).send();
      }

      const nodeDTO = nodeOrError.getValue();
      return res.json(nodeDTO).status(201);
    } catch (e) {
      return next(e);
    }
  }

}
