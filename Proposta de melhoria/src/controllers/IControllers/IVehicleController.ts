import { Request, Response, NextFunction } from "express";

export default interface IVehicleController {
  getAllVehicles(req: Request, res: Response, next: NextFunction);
  getVehicleById(req: Request, res: Response, next: NextFunction);
  createVehicle(req: Request, res: Response, next: NextFunction);
  updateVehicle(req: Request, res: Response, next: NextFunction);
}
