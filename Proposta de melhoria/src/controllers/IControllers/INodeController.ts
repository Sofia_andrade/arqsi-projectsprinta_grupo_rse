import { Request, Response, NextFunction } from "express";

export default interface INodeController {
  getAllNodes(req: Request, res: Response, next: NextFunction);
  getNodeById(req: Request, res: Response, next: NextFunction);
  createNode(req: Request, res: Response, next: NextFunction);
  updateNode(req: Request, res: Response, next: NextFunction);
}
