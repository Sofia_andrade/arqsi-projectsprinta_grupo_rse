import { Request, Response, NextFunction } from "express";

export default interface IDriverController {
  getAllDrivers(req: Request, res: Response, next: NextFunction);
  getDriverById(req: Request, res: Response, next: NextFunction);
  createDriver(req: Request, res: Response, next: NextFunction);
  updateDriver(req: Request, res: Response, next: NextFunction);
}
