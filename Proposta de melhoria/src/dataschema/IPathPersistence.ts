import { Mongoose } from "mongoose";

export interface IPathPersistence {    
    linepathkey: string;
    path: [
        {
            pathkey: string;
            isempty: boolean;
            pathnodes: [
                {
                    pathnodekey: string;
                    node: string;
                    duration: number;
                    distance: number
                }
            ]
        }];
    orientation: string;
  }
  