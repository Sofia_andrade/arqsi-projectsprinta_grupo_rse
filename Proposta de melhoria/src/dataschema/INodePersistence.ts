export interface INodePersistence {
  id: string;
  name: string;
  ShortName: string;
  IsDepot: boolean;
  IsRelieafPoint: boolean;
  Nodekey: string;
  Latitude: string;
  Longitude: string;
  CrewTravelTime: string;
}
