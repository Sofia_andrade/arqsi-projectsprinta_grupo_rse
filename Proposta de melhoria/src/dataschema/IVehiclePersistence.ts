export interface IVehiclePersistence {
  id: string;
  vehicletype: string;
  name: string;
  autonomy: number;
  cost: number;
  averagespeed: number;
  energysource: string;
  consuption: number;
  emissions: number;
}
