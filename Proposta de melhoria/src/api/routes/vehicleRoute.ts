import { Router } from "express";
import { celebrate, Joi } from "celebrate";

import { Container } from "typedi";
import IVehicleController from "../../controllers/IControllers/IVehicleController";

import config from "../../../config";

const route = Router();

export default (app: Router) => {
  app.use("/vehicles", route);

  const ctrl = Container.get(config.controllers.vehicle.name) as IVehicleController;

  route.post(
    "",
    celebrate({
      body: Joi.object({
        name: Joi.string().required()
      })
    }),
    (req, res, next) => ctrl.createVehicle(req, res, next)
  );

  route.put(
    "",
    celebrate({
      body: Joi.object({
        id: Joi.string().required(),
        name: Joi.string().required()
      })
    }),
    (req, res, next) => ctrl.updateVehicle(req, res, next)
  );
};
