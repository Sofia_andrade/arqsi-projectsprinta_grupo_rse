import { Router } from "express";
import { celebrate, Joi } from "celebrate";

import { Container } from "typedi";
import IDriverController from "../../controllers/IControllers/IDriverController";

import config from "../../../config";

const route = Router();

export default (app: Router) => {
  app.use("/drivers", route);

  const ctrl = Container.get(config.controllers.driver.name) as IDriverController;

  route.post(
    "",
    celebrate({
      body: Joi.object({
        name: Joi.string().required()
      })
    }),
    (req, res, next) => ctrl.createDriver(req, res, next)
  );

  route.put(
    "",
    celebrate({
      body: Joi.object({
        id: Joi.string().required(),
        name: Joi.string().required()
      })
    }),
    (req, res, next) => ctrl.updateDriver(req, res, next)
  );
};
