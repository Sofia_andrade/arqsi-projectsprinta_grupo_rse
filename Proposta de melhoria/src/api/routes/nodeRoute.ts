import { Router } from "express";
import { celebrate, Joi } from "celebrate";

import { Container } from "typedi";
import INodeController from "../../controllers/IControllers/INodeController";

import config from "../../../config";

const route = Router();

export default (app: Router) => {
  app.use("/nodes", route);

  const ctrl = Container.get(config.controllers.node.name) as INodeController;

  route.post(
    "",
    celebrate({
      body: Joi.object({
        Name: Joi.string().required(),
        ShortName: Joi.string().required(),
        IsDepot: Joi.boolean(),
        IsRelieafPoint: Joi.boolean(),
        Nodekey: Joi.string().required(),
        Latitude: Joi.number().required(),
        Longitude: Joi.number().required(),
        CrewTravelTime: Joi.string().required(),
      })
    }),
    (req, res, next) => ctrl.createNode(req, res, next)
  );

  route.put(
    "",
    celebrate({
      body: Joi.object({
        id: Joi.string().required(),
        Name: Joi.string().required(),
        ShortName: Joi.string().required(),
        IsDepot: Joi.boolean(),
        IsRelieafPoint: Joi.boolean(),
        Nodekey: Joi.string().required(),
        Latitude: Joi.number().required(),
        Longitude: Joi.number().required(),
        CrewTravelTime: Joi.string().required(),
      })
    }),
    (req, res, next) => ctrl.updateNode(req, res, next)
  );

  route.get(
    "",
    celebrate({
      body: Joi.object({
        id: Joi.string().required(),
        Name: Joi.string().required(),
        ShortName: Joi.string().required(),
        IsDepot: Joi.boolean(),
        IsRelieafPoint: Joi.boolean(),
        Nodekey: Joi.string().required(),
        Latitude: Joi.number().required(),
        Longitude: Joi.number().required(),
        CrewTravelTime: Joi.string().required(),
      })
    }),
    (req, res, next) => ctrl.getAllNodes(req, res, next)
  );

  route.getbyid(
    "",
    celebrate({
      body: Joi.object({
        id: Joi.string().required(),
        Name: Joi.string().required(),
        ShortName: Joi.string().required(),
        IsDepot: Joi.boolean(),
        IsRelieafPoint: Joi.boolean(),
        Nodekey: Joi.string().required(),
        Latitude: Joi.number().required(),
        Longitude: Joi.number().required(),
        CrewTravelTime: Joi.string().required(),
      })
    }),
    (req, res, next) => ctrl.getNodeById(req, res, next)
  );
};
