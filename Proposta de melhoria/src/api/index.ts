import { Router } from "express";
import driver from "./routes/driverRoute";
//import auth from './routes/authRoute';
import node from "./routes/nodeRoute";
import vehicle from "./routes/vehicleRoute";

export default () => {
  const app = Router();

  //auth(app);
  vehicle(app);
  driver(app);
  node(app);

  return app;
};
