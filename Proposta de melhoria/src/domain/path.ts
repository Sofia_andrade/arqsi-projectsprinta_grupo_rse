import { AggregateRoot } from "../core/domain/AggregateRoot";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { Guard } from "../core/logic/Guard";
import { Result } from "../core/logic/Result";
import { PathId } from "./pathId";
import IPathDTO from "../dto/IPathDTO";
import { SinglePath } from "./singlepath";

interface PathProps {
  path: SinglePath;
  orientation: string;
}

export class Path extends AggregateRoot<PathProps> {
  get id(): UniqueEntityID {
    return this._id;
  }

  get pathId(): PathId {
    return PathId.create(this.id);
  }

  get orientation(): string {
    return this.props.orientation;
  }

  get path(): SinglePath {
    return this.props.path;
  }
  set path(value: SinglePath) {
    this.props.path = value;
  }
  private constructor(props: PathProps, id?: UniqueEntityID) {
    super(props, id);
  }

  public static create(props: PathProps, id?: UniqueEntityID): Result<Path> {
    const guardedProps = [
      { argument: props.path, argumentName: "path" },
      { argument: props.orientation, argumentName: "orientation" },
    ];

    const guardResult = Guard.againstNullOrUndefinedBulk(guardedProps);

    if (!guardResult.succeeded) {
      return Result.fail<Path>(guardResult.message);
    } else {
      const path = new Path(
        {
          ...props,
        },
        id
      );
      return Result.ok<Path>(path);
    }
  }
}
