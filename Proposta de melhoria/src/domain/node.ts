import { AggregateRoot } from "../core/domain/AggregateRoot";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";

import { Result } from "../core/logic/Result";
import { NodeId } from "./nodeId";

import INodeDTO from "../dto/INodeDTO";

interface NodeProps {
  name: string;
}

export class Node extends AggregateRoot<NodeProps> {
  get id(): UniqueEntityID {
    return this._id;
  }

  get nodeId(): NodeId {
    return NodeId.create(this.id);
  }

  get name(): string {
    return this.props.name;
  }

  set name(value: string) {
    this.props.name = value;
  }
  private constructor(props: NodeProps, id?: UniqueEntityID) {
    super(props, id);
  }

  public static create(nodeDTO: INodeDTO, id?: UniqueEntityID): Result<Node> {
    const name = nodeDTO.name;

    if (!!name === false || name.length === 0) {
      return Result.fail<Node>("Must provide a node name");
    } else {
      const node = new Node({ name: name }, id);
      return Result.ok<Node>(node);
    }
  }
}
