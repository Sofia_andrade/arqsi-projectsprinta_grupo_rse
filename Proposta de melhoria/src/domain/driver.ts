import { AggregateRoot } from "../core/domain/AggregateRoot";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";

import { Result } from "../core/logic/Result";
import { DriverId } from "./driverId";

import IDriverDTO from "../dto/IDriverDTO";

interface DriverProps {
  name: string;
}

export class Driver extends AggregateRoot<DriverProps> {
  get id(): UniqueEntityID {
    return this._id;
  }

  get driverId(): DriverId {
    return DriverId.create(this.id);
  }

  get name(): string {
    return this.props.name;
  }

  set name(value: string) {
    this.props.name = value;
  }
  private constructor(props: DriverProps, id?: UniqueEntityID) {
    super(props, id);
  }

  public static create(driverDTO: IDriverDTO, id?: UniqueEntityID): Result<Driver> {
    const name = driverDTO.name;

    if (!!name === false || name.length === 0) {
      return Result.fail<Driver>("Must provide a driver name");
    } else {
      const driver = new Driver({ name: name }, id);
      return Result.ok<Driver>(driver);
    }
  }
}
