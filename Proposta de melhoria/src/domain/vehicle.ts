import { AggregateRoot } from "../core/domain/AggregateRoot";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";

import { Result } from "../core/logic/Result";
import { VehicleId } from "./vehicleId";

import IVehicleDTO from "../dto/IVehicleDTO";

interface VehicleProps {
  name: string;
}

export class Vehicle extends AggregateRoot<VehicleProps> {
  get id(): UniqueEntityID {
    return this._id;
  }

  get vehicleId(): VehicleId {
    return VehicleId.create(this.id);
  }

  get name(): string {
    return this.props.name;
  }

  set name(value: string) {
    this.props.name = value;
  }
  private constructor(props: VehicleProps, id?: UniqueEntityID) {
    super(props, id);
  }

  public static create(vehicleDTO: IVehicleDTO, id?: UniqueEntityID): Result<Vehicle> {
    const name = vehicleDTO.name;

    if (!!name === false || name.length === 0) {
      return Result.fail<Vehicle>("Must provide a vehicle name");
    } else {
      const vehicle = new Vehicle({ name: name }, id);
      return Result.ok<Vehicle>(vehicle);
    }
  }
}
