import { UniqueEntityID } from "../core/domain/UniqueEntityID";

export class NodeId extends UniqueEntityID {}
