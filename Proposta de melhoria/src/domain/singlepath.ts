import { ValueObject } from "../core/domain/ValueObject";
import { Result } from "../core/logic/Result";
import { Guard } from "../core/logic/Guard";
import { PathNode } from "./pathnodes";

interface SinglePathProps {
  pathkey: string;
  isempty: boolean;
  pathnodes: PathNode;
}

export class SinglePath extends ValueObject<SinglePathProps> {
  get pathkey(): string {
    return this.props.pathkey;
  }
  get isempty(): boolean {
    return this.props.isempty;
  }
  get pathnodes(): PathNode {
    return this.props.pathnodes;
  }

  private constructor(props: SinglePathProps) {
    super(props);
  }

  public static create(props: SinglePathProps): Result<SinglePath> {
    const guardedProps = [
      { argument: props.pathkey, argumentName: "pathkey" },
      { argument: props.isempty, argumentName: "isempty" },
      { argument: props.pathnodes, argumentName: "pathnodes" },
    ];

    const guardResult = Guard.againstNullOrUndefinedBulk(guardedProps);

    if (!guardResult.succeeded) {
      return Result.fail<SinglePath>(guardResult.message);
    } else {
      const singlepath = new SinglePath({ ...props });
      return Result.ok<SinglePath>(singlepath);
    }
  }
}
