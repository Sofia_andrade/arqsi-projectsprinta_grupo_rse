import { ValueObject } from "../core/domain/ValueObject";
import { Result } from "../core/logic/Result";
import { Guard } from "../core/logic/Guard";

interface PathNodeProps {
    pathnodekey: string;
    node: string;
    duration: number;
    distance: number
}

export class PathNode extends ValueObject<PathNodeProps> {
  get pathnodekey (): string {
    return this.props.pathnodekey;
  }
  get node (): string {
    return this.props.node;
  }
  get duration (): number {
    return this.props.duration;
  }
  get distance (): number {
    return this.props.distance;
  }
  
  private constructor (props: PathNodeProps) {
    super(props);
  }

  public static create (props: PathNodeProps): Result<PathNode> {
    const guardedProps = [
        { argument: props.pathnodekey, argumentName: 'pathnodekey' },
        { argument: props.node, argumentName: 'node' },
        { argument: props.duration, argumentName: 'duration' },
        { argument: props.distance, argumentName: 'distance' }
      ];
  
      const guardResult = Guard.againstNullOrUndefinedBulk(guardedProps);
  
    if (!guardResult.succeeded) {
      return Result.fail<PathNode>(guardResult.message);
    } 
    else {
        const pathnode = new PathNode({...props});
      return Result.ok<PathNode>(pathnode)
    }
  }
}