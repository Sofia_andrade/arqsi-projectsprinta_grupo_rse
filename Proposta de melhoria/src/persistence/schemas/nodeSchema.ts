import { INodePersistence } from "../../dataschema/INodePersistence";
import mongoose from "mongoose";

const NodeSchema = new mongoose.Schema(
  {
    Nodekey: {
      type: String,

      required: true,

      unique: true
    },
    name: {
      type: String,

      required: true
    },
    Latitude: {
      type: String,

      required: true
    },
    Longitude: {
      type: String,

      required: true
    },
    ShortName: {
      type: String,

      required: true,

      unique: true,

      uppercase: true
    },
    IsDepot: {
      type: Boolean
    },
    IsRelieafPoint: {
      type: Boolean
    }
  },
  {
    timestamps: true
  }
);

export default mongoose.model<INodePersistence & mongoose.Document>(
  "Node",
  NodeSchema
);
