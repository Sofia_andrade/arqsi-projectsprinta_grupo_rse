import { IVehiclePersistence } from "../../dataschema/IVehiclePersistence";
import mongoose from "mongoose";

const VehicleSchema = new mongoose.Schema(
  {
    Nodekey: {
      type: String,

      required: true,

      unique: true
    },
    name: {
      type: String,

      required: true
    },
    Latitude: {
      type: String,

      required: true
    },
    Longitude: {
      type: String,

      required: true
    },
    ShortName: {
      type: String,

      required: true,

      unique: true,

      uppercase: true
    },
    IsDepot: {
      type: Boolean
    },
    IsRelieafPoint: {
      type: Boolean
    }
  },
  {
    timestamps: true
  }
);

export default mongoose.model<IVehiclePersistence & mongoose.Document>(
  "Vehicle",
  VehicleSchema
);
