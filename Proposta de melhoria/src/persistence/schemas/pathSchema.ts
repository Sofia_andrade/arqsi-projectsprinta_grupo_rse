import { IPathPersistence } from "../../dataschema/IPathPersistence";
import mongoose from "mongoose";

//NOTE: criação Schema auxiliar: Segmento de Nó
//TO FIX: por apurar dúvida de negócio na implementação: lógica sequencial A->B->C->D OU [A,B]->[B,C]->[C,D]. Neste momento está a primeira.
//TO FIX: em falta validações das properties, como lenght

var pathnodeSchema = new mongoose.Schema({
  pathnodekey: {
    type: String,
    required: true,
    unique: true,
  },
  node: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Node",
  },
  duration: {
    type: Number,
  },
  distance: {
    type: Number,
  },
});

//NOTE:criação schema para Percurso
//
const singlepathSchema = new mongoose.Schema({
  pathkey: {
    type: String,
    required: true,
    unique: true,
  },
  isempty: {
    type: Boolean,
  },
  pathnodes: [pathnodeSchema],
});

//NOTE: criação schema PERCURSO DE LINHA
//TO FIX: linepathkey deve ser required e unique? supostamente sim (todas as linhas tem pelo menos 1 percurso)
//TO FIX: orientation só deveria permitir 2 tipos de valores (Go/Return)

const PathSchema = new mongoose.Schema({
  linepathkey: {
    type: String,
    required: true,
    unique: true,
  },
  path: [
    {
      type: singlepathSchema,
    },
  ],
  orientation: {
    type: String,
    uppercase: true,
  },
});

export default mongoose.model<IPathPersistence & mongoose.Document>(
  "Path",
  PathSchema
);
