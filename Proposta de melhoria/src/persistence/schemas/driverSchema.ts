import { IDriverPersistence } from "../../dataschema/IDriverPersistence";
import mongoose from "mongoose";

const DriverSchema = new mongoose.Schema(
  {
    Nodekey: {
      type: String,

      required: true,

      unique: true
    },
    name: {
      type: String,

      required: true
    },
    Latitude: {
      type: String,

      required: true
    },
    Longitude: {
      type: String,

      required: true
    },
    ShortName: {
      type: String,

      required: true,

      unique: true,

      uppercase: true
    },
    IsDepot: {
      type: Boolean
    },
    IsRelieafPoint: {
      type: Boolean
    }
  },
  {
    timestamps: true
  }
);

export default mongoose.model<IDriverPersistence & mongoose.Document>(
  "Driver",
  DriverSchema
);
