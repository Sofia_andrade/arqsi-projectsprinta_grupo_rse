import { Service, Inject } from "typedi";

import IDriverRepo from "../services/IRepos/IDriverRepo";
import { Driver } from "../domain/driver";
import { DriverId } from "../domain/driverId";
import { DriverMap } from "../mappers/DriverMap";

import { Document, Model } from "mongoose";
import { IDriverPersistence } from "../dataschema/IDriverPersistence";

@Service()
export default class DriverRepo implements IDriverRepo {
  private models: any;

  constructor(
    @Inject("driverSchema") private driverSchema: Model<IDriverPersistence & Document>
  ) {}

  private createBaseQuery(): any {
    return {
      where: {}
    };
  }

  public async exists(driverId: DriverId | string): Promise<boolean> {
    const idX =
      driverId instanceof DriverId ? (<DriverId>driverId).id.toValue() : driverId;

    const query = { domainId: idX };
    const driverDocument = await this.driverSchema.findOne(query);

    return !!driverDocument === true;
  }

  public async save(driver: Driver): Promise<Driver> {
    const query = { domainId: driver.id.toString() };

    const driverDocument = await this.driverSchema.findOne(query);

    try {
      if (driverDocument === null) {
        const rawDriver: any = DriverMap.toPersistence(driver);

        const driverCreated = await this.driverSchema.create(rawDriver);

        return DriverMap.toDomain(driverCreated);
      } else {
        driverDocument.name = driver.name;
        await driverDocument.save();

        return driver;
      }
    } catch (err) {
      throw err;
    }
  }

  public async findByDomainId(driverId: DriverId | string): Promise<Driver> {
    const query = { domainId: driverId };
    const driverRecord = await this.driverSchema.findOne(query);

    if (driverRecord != null) {
      return DriverMap.toDomain(driverRecord);
    } else return null;
  }
}
