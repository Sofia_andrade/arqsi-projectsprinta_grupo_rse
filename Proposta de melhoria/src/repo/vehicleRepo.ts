import { Service, Inject } from "typedi";

import IVehicleRepo from "../services/IRepos/IVehicleRepo";
import { Vehicle } from "../domain/vehicle";
import { VehicleId } from "../domain/vehicleId";
import { VehicleMap } from "../mappers/VehicleMap";

import { Document, Model } from "mongoose";
import { IVehiclePersistence } from "../dataschema/IVehiclePersistence";

@Service()
export default class VehicleRepo implements IVehicleRepo {
  private models: any;

  constructor(
    @Inject("vehicleSchema") private vehicleSchema: Model<IVehiclePersistence & Document>
  ) {}

  private createBaseQuery(): any {
    return {
      where: {}
    };
  }

  public async exists(vehicleId: VehicleId | string): Promise<boolean> {
    const idX =
      vehicleId instanceof VehicleId ? (<VehicleId>vehicleId).id.toValue() : vehicleId;

    const query = { domainId: idX };
    const vehicleDocument = await this.vehicleSchema.findOne(query);

    return !!vehicleDocument === true;
  }

  public async save(vehicle: Vehicle): Promise<Vehicle> {
    const query = { domainId: vehicle.id.toString() };

    const vehicleDocument = await this.vehicleSchema.findOne(query);

    try {
      if (vehicleDocument === null) {
        const rawVehicle: any = VehicleMap.toPersistence(vehicle);

        const vehicleCreated = await this.vehicleSchema.create(rawVehicle);

        return VehicleMap.toDomain(vehicleCreated);
      } else {
        vehicleDocument.name = vehicle.name;
        await vehicleDocument.save();

        return vehicle;
      }
    } catch (err) {
      throw err;
    }
  }

  public async findByDomainId(vehicleId: VehicleId | string): Promise<Vehicle> {
    const query = { domainId: vehicleId };
    const vehicleRecord = await this.vehicleSchema.findOne(query);

    if (vehicleRecord != null) {
      return VehicleMap.toDomain(vehicleRecord);
    } else return null;
  }
}
