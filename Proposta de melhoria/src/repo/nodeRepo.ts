import { Service, Inject } from "typedi";

import INodeRepo from "../services/IRepos/INodeRepo";
import { Node } from "../domain/node";
import { NodeId } from "../domain/nodeId";
import { NodeMap } from "../mappers/NodeMap";

import { Document, Model } from "mongoose";
import { INodePersistence } from "../dataschema/INodePersistence";

@Service()
export default class NodeRepo implements INodeRepo {
  private models: any;

  constructor(
    @Inject("nodeSchema") private nodeSchema: Model<INodePersistence & Document>
  ) {}

  private createBaseQuery(): any {
    return {
      where: {}
    };
  }

  public async exists(nodeId: NodeId | string): Promise<boolean> {
    const idX =
      nodeId instanceof NodeId ? (<NodeId>nodeId).id.toValue() : nodeId;

    const query = { domainId: idX };
    const nodeDocument = await this.nodeSchema.findOne(query);

    return !!nodeDocument === true;
  }

  public async save(node: Node): Promise<Node> {
    const query = { domainId: node.id.toString() };

    const nodeDocument = await this.nodeSchema.findOne(query);

    try {
      if (nodeDocument === null) {
        const rawNode: any = NodeMap.toPersistence(node);

        const nodeCreated = await this.nodeSchema.create(rawNode);

        return NodeMap.toDomain(nodeCreated);
      } else {
        nodeDocument.name = node.name;
        await nodeDocument.save();

        return node;
      }
    } catch (err) {
      throw err;
    }
  }

  public async findByDomainId(nodeId: NodeId | string): Promise<Node> {
    const query = { domainId: nodeId };
    const nodeRecord = await this.nodeSchema.findOne(query);

    if (nodeRecord != null) {
      return NodeMap.toDomain(nodeRecord);
    } else return null;
  }
}
