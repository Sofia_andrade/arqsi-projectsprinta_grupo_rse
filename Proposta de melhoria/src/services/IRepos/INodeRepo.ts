import { Repo } from "../../core/infra/Repo";
import { Node } from "../../domain/node";
import { NodeId } from "../../domain/nodeId";

export default interface INodeRepo extends Repo<Node> {
  save(node: Node): Promise<Node>;
  findByDomainId(nodeId: NodeId | string): Promise<Node>;

  //findByIds (nodesIds: NodeId[]): Promise<Node[]>;
  //saveCollection (nodes: Node[]): Promise<Node[]>;
  //removeByNodeIds (nodes: NodeId[]): Promise<any>
}
