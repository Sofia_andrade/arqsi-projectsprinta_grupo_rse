import { Repo } from "../../core/infra/Repo";
import { Vehicle } from "../../domain/vehicle";
import { VehicleId } from "../../domain/vehicleId";

export default interface IVehicleRepo extends Repo<Vehicle> {
  save(vehicle: Vehicle): Promise<Vehicle>;
  findByDomainId(vehicleId: VehicleId | string): Promise<Vehicle>;

  //findByIds (vehiclesIds: VehicleId[]): Promise<Vehicle[]>;
  //saveCollection (vehicles: Vehicle[]): Promise<Vehicle[]>;
  //removeByVehicleIds (vehicles: VehicleId[]): Promise<any>
}
