import { Result } from "../../core/logic/Result";
import IVehicleDTO from "../../dto/IVehicleDTO";

export default interface IVehicleService {
  createVehicle(vehicleDTO: IVehicleDTO): Promise<Result<IVehicleDTO>>;
  updateVehicle(vehicleDTO: IVehicleDTO): Promise<Result<IVehicleDTO>>;
}
