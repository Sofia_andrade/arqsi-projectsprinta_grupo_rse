import { Result } from "../../core/logic/Result";
import INodeDTO from "../../dto/INodeDTO";

export default interface INodeService {
  createNode(nodeDTO: INodeDTO): Promise<Result<INodeDTO>>;
  updateNode(nodeDTO: INodeDTO): Promise<Result<INodeDTO>>;
  getAllNodes(nodeDTO: INodeDTO): Promise<Result<INodeDTO>>;
  getNodeById(nodeDTO: INodeDTO): Promise<Result<INodeDTO>>;
}
