import { Result } from "../../core/logic/Result";
import IDriverDTO from "../../dto/IDriverDTO";

export default interface IDriverService {
  createDriver(driverDTO: IDriverDTO): Promise<Result<IDriverDTO>>;
  updateDriver(driverDTO: IDriverDTO): Promise<Result<IDriverDTO>>;
}
