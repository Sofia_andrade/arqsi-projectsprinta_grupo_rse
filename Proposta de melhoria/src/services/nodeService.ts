import { Service, Inject } from "typedi";
import config from "../../config";
import INodeDTO from "../dto/INodeDTO";
import { Node } from "../domain/node";
import INodeRepo from "../services/IRepos/INodeRepo";
import INodeService from "./IServices/INodeService";
import { Result } from "../core/logic/Result";
import { NodeMap } from "../mappers/NodeMap";

@Service()
export default class NodeService implements INodeService {
  constructor(@Inject(config.repos.node.name) private nodeRepo: INodeRepo) {}

  public async createNode(nodeDTO: INodeDTO): Promise<Result<INodeDTO>> {
    try {
      const nodeOrError = await Node.create(nodeDTO);

      if (nodeOrError.isFailure) {
        return Result.fail<INodeDTO>(nodeOrError.errorValue());
      }

      const nodeResult = nodeOrError.getValue();

      await this.nodeRepo.save(nodeResult);

      const nodeDTOResult = NodeMap.toDTO(nodeResult) as INodeDTO;
      return Result.ok<INodeDTO>(nodeDTOResult);
    } catch (e) {
      throw e;
    }
  }

  public async getAllNodes(nodeDTO: INodeDTO): Promise<Result<INodeDTO>> {
    try {
      const nodeOrError = await Node.getAll();

      if (nodeOrError.isFailure) {
        return Result.fail<INodeDTO>(nodeOrError.errorValue());
      }

      const nodeResult = nodeOrError.getValue();

      await this.nodeRepo.save(nodeResult);

      const nodeDTOResult = NodeMap.toDTO(nodeResult) as INodeDTO;
      return Result.ok<INodeDTO>(nodeDTOResult);
    } catch (e) {
      throw e;
    }
  }
//alterar código
  public async getNodeById(nodeDTO: INodeDTO): Promise<Result<INodeDTO>> {
    try {
      const node = await this.nodeRepo.findByIds([])
        ;

      if (node === null) {
        return Result.fail<INodeDTO>("Role not found");
      } else {
        node.name = nodeDTO.name;
        await this.nodeRepo.save(node);

        const nodeDTOResult = NodeMap.toDTO(node) as INodeDTO;
        return Result.ok<INodeDTO>(nodeDTOResult);
      }
    } catch (e) {
      throw e;
    }
  }

  public async updateNode(nodeDTO: INodeDTO): Promise<Result<INodeDTO>> {
    try {
      const node = await this.nodeRepo.findByDomainId(nodeDTO.id);

      if (node === null) {
        return Result.fail<INodeDTO>("Role not found");
      } else {
        node.name = nodeDTO.name;
        await this.nodeRepo.save(node);

        const nodeDTOResult = NodeMap.toDTO(node) as INodeDTO;
        return Result.ok<INodeDTO>(nodeDTOResult);
      }
    } catch (e) {
      throw e;
    }
  }
}
