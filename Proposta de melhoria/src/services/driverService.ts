import { Service, Inject } from "typedi";
import config from "../../config";
import IDriverDTO from "../dto/IDriverDTO";
import { Driver } from "../domain/driver";
import IDriverRepo from "../services/IRepos/IDriverRepo";
import IDriverService from "./IServices/IDriverService";
import { Result } from "../core/logic/Result";
import { DriverMap } from "../mappers/DriverMap";

@Service()
export default class DriverService implements IDriverService {
  constructor(
    @Inject(config.repos.driver.name) private driverRepo: IDriverRepo
  ) {}

  public async createDriver(
    driverDTO: IDriverDTO
  ): Promise<Result<IDriverDTO>> {
    try {
      const driverOrError = await Driver.create(driverDTO);

      if (driverOrError.isFailure) {
        return Result.fail<IDriverDTO>(driverOrError.errorValue());
      }

      const driverResult = driverOrError.getValue();

      await this.driverRepo.save(driverResult);

      const driverDTOResult = DriverMap.toDTO(driverResult) as IDriverDTO;
      return Result.ok<IDriverDTO>(driverDTOResult);
    } catch (e) {
      throw e;
    }
  }

  public async updateDriver(
    driverDTO: IDriverDTO
  ): Promise<Result<IDriverDTO>> {
    try {
      const driver = await this.driverRepo.findByDomainId(driverDTO.id);

      if (driver === null) {
        return Result.fail<IDriverDTO>("Role not found");
      } else {
        driver.name = driverDTO.name;
        await this.driverRepo.save(driver);

        const driverDTOResult = DriverMap.toDTO(driver) as IDriverDTO;
        return Result.ok<IDriverDTO>(driverDTOResult);
      }
    } catch (e) {
      throw e;
    }
  }
}
