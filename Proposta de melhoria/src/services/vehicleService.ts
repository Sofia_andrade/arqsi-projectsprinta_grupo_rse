import { Service, Inject } from "typedi";
import config from "../../config";
import IVehicleDTO from "../dto/IVehicleDTO";
import { Vehicle } from "../domain/vehicle";
import IVehicleRepo from "../services/IRepos/IVehicleRepo";
import IVehicleService from "./IServices/IVehicleService";
import { Result } from "../core/logic/Result";
import { VehicleMap } from "../mappers/VehicleMap";

@Service()
export default class VehicleService implements IVehicleService {
  constructor(@Inject(config.repos.vehicle.name) private vehicleRepo: IVehicleRepo) {}

  public async createVehicle(vehicleDTO: IVehicleDTO): Promise<Result<IVehicleDTO>> {
    try {
      const vehicleOrError = await Vehicle.create(vehicleDTO);

      if (vehicleOrError.isFailure) {
        return Result.fail<IVehicleDTO>(vehicleOrError.errorValue());
      }

      const vehicleResult = vehicleOrError.getValue();

      await this.vehicleRepo.save(vehicleResult);

      const vehicleDTOResult = VehicleMap.toDTO(vehicleResult) as IVehicleDTO;
      return Result.ok<IVehicleDTO>(vehicleDTOResult);
    } catch (e) {
      throw e;
    }
  }

  public async updateVehicle(vehicleDTO: IVehicleDTO): Promise<Result<IVehicleDTO>> {
    try {
      const vehicle = await this.vehicleRepo.findByDomainId(vehicleDTO.id);

      if (vehicle === null) {
        return Result.fail<IVehicleDTO>("Role not found");
      } else {
        vehicle.name = vehicleDTO.name;
        await this.vehicleRepo.save(vehicle);

        const vehicleDTOResult = VehicleMap.toDTO(vehicle) as IVehicleDTO;
        return Result.ok<IVehicleDTO>(vehicleDTOResult);
      }
    } catch (e) {
      throw e;
    }
  }
}
