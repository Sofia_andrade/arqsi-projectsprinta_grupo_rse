export default interface IPathDTO {
    id: string;
    path: [
        {
            pathkey: string;
            isempty: boolean;
            pathnodes: [
                {
                    pathnodekey: string;
                    node: string;
                    duration: number;
                    distance: number
                }
            ]
        }];
    orientation: string;
  }