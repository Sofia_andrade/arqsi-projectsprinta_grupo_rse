export default interface IDriverDTO {
  id: string;
  Code: string;
  Description: string;
}
