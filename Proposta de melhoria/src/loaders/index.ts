import expressLoader from "./express";
import dependencyInjectorLoader from "./dependencyInjector";
import mongooseLoader from "./mongoose";
import Logger from "./logger";

import config from "../../config";

export default async ({ expressApp }) => {
  const mongoConnection = await mongooseLoader();
  Logger.info("✌️ DB loaded and connected!");

  const nodeSchema = {
    // compare with the approach followed in repos and services
    name: "nodeSchema",
    schema: "../persistence/schemas/nodeSchema"
  };

  const nodeController = {
    name: config.controllers.node.name,
    path: config.controllers.node.path
  };

  const nodeRepo = {
    name: config.repos.node.name,
    path: config.repos.node.path
  };

  const nodeService = {
    name: config.services.node.name,
    path: config.services.node.path
  };
  //entidade PATH
  const pathSchema = {
    name: "pathSchema",
    schema: "../persistence/schemas/pathSchema"
  };

  const pathController = {
    name: config.controllers.path.name,
    path: config.controllers.path.path
  };

  const pathRepo = {
    name: config.repos.path.name,
    path: config.repos.path.path
  };

  const pathService = {
    name: config.services.path.name,
    path: config.services.path.path
  };

  //entidade Vehicle
  const vehicleSchema = {
    name: config.controllers.vehicle.name,
    path: config.controllers.vehicle.path
  };

  const vehicleController = {
    name: config.controllers.vehicle.name,
    path: config.controllers.vehicle.path
  };

  const vehicleRepo = {
    name: config.repos.vehicle.name,
    path: config.repos.vehicle.path
  };

  const vehicleService = {
    name: config.services.vehicle.name,
    path: config.services.vehicle.path
  };

  //entidade Driver
  const driverSchema = {
    name: config.controllers.driver.name,
    path: config.controllers.driver.path
  };

  const driverController = {
    name: config.controllers.driver.name,
    path: config.controllers.driver.path
  };

  const driverRepo = {
    name: config.repos.driver.name,
    path: config.repos.driver.path
  };

  const driverService = {
    name: config.services.driver.name,
    path: config.services.driver.path
  };

  await dependencyInjectorLoader({
    mongoConnection,
    schemas: [nodeSchema, pathSchema, vehicleSchema, driverSchema],
    controllers: [
      nodeController,
      pathController,
      vehicleController,
      driverController
    ],
    repos: [nodeRepo, pathRepo, vehicleRepo, driverRepo],
    services: [nodeService, pathService, vehicleService, driverService]
  });
  Logger.info("✌️ Schemas, Controllers, Repositories, Services, etc. loaded");

  await expressLoader({
    app: expressApp
  });
  Logger.info("✌️ Express loaded");
};
