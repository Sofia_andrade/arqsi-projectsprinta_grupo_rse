import dotenv from "dotenv";

// Set the NODE_ENV to 'development' by default
process.env.NODE_ENV = process.env.NODE_ENV || "development";

const envFound = dotenv.config();
if (!envFound) {
  // This error should crash whole process

  throw new Error("⚠️  Couldn't find .env file  ⚠️");
}

export default {
  /**
   * Your favorite port
   */
  port: parseInt(process.env.PORT, 10),

  /**
   * That long string from mlab
   */
  databaseURL: process.env.DB_URI,

  /**
   * Your secret sauce
   */
  jwtSecret: process.env.JWT_SECRET,

  /**
   * Used by winston logger
   */
  logs: {
    level: process.env.LOG_LEVEL || "silly"
  },

  /**
   * API configs
   */
  api: {
    prefix: "/api"
  },

  controllers: {
    node: {
      name: "NodeController",
      path: "../controllers/nodeController"
    },
    path: {
      name: "PathController",
      path: "../controllers/pathController"
    },
    line: {
      name: "LineController",
      path: "../controllers/lineController"
    },
    vehicle: {
      name: "VehicleController",
      path: "../controllers/vehicleController"
    },
    driver: {
      name: "DriverController",
      path: "../controllers/driverController"
    }
  },

  repos: {
    node: {
      name: "NodeRepo",
      path: "../repo/nodeRepo"
    },
    path: {
      name: "PathRepo",
      path: "../repo/pathRepo"
    },
    line: {
      name: "LineRepo",
      path: "../repo/lineRepo"
    },
    vehicle: {
      name: "VehicleRepo",
      path: "../repo/vehicleRepo"
    },
    driver: {
      name: "DriverRepo",
      path: "../repo/driverRepo"
    }
  },

  services: {
    node: {
      name: "NodeService",
      path: "../services/nodeService"
    },
    path: {
      name: "PathService",
      path: "../services/pathService"
    },
    line: {
      name: "LineService",
      path: "../services/lineService"
    },
    vehicle: {
      name: "VehicleService",
      path: "../services/vehicleService"
    },
    driver: {
      name: "DriverService",
      path: "../services/driverService"
    }
  }
};
