**Tentativa de adaptação à arquitetura onion**

Como falamos via Teams, enviamos os nosso trabalho no estado actual para sua apreciação.
Do trabalho apresentado na sprint A para este enviado agora, fizemos as seguintes alterações:
-Adaptamos o código que o professor Nuno disponibilizou no bitbucket para o nosso problema;
-Desenvolvemos a arquitectura desde as "routes" até à persistência para: drivers, vehicles, paths e nodes.

Esta versão não está completa e com erros, mas vamos deixar por aqui e passar para o SPA do sprint B.
