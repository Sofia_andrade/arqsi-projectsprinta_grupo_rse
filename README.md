**Notas relativas ao Sprint A**

Neste Sprint não nos foi possível completar todos os requisitos funcionais e não funcionais.
Temos consciência que a nossa app não apresenta ainda uma arquitetura clara e adequada.

O nosso foco inicial neste sprint, e por ausência de bases, foi familiarizar-nos com o Node.js e as várias ferramentas necessárias (Visual Studio Code, Visual Paradigm, BitBucket, Postman, Mondo DB Cloud/Mongo DB Compass, Heroku).

**Sobre o nosso grupo**

Estamos a frequentar a UC de ARQSI como precedência para a realização do mestrado em Eng.Informática - Sistemas de Informação e Conhecimento.

Grupo:

Eduardo Monteiro (1200151)

Rui Fernandes (1200183)

Sofia Andrade (1200186)

---

## Design

Iremos adoptar a complementaridade do modelo C4 com as vistas 4+1.

Diagrama de Contexto (nível 1), Vista Lógica: [C1-L](/DESIGN/Nível_1)

Diagrama de Containers (nível 2), Vista Lógica: [C2-L](/DESIGN/Nível_2)

Diagrama de Componentes (nível 3), Vista Lógica: [C3-L](/DESIGN/Nível_2)

*Neste caso apresentamos 2 diagramas. Um é a representação do que queremos nos próximos sprints (TO-BE) a outra representa a versão atual da nossa app (AS-IS)*

---

## Postman

### Master Data Rede (localhost:8080)

#### Pedidos GET

##### Listar Nós (US7)

```
GET localhost:8080/nodes
GET localhost:8080/nodes/_id
```

##### Listar Linhas (US8)

```
GET localhost:8080/lines
GET localhost:8080/lines/_id
```

##### Listar Percursos (US9)

```
GET localhost:8080/path
GET localhost:8080/path/_id
```

#### Pedidos POST

##### Criar Nós (US2)

```
POST localhost:8080/nodes
```

- Exemplo do Body:

```
{
    "Nodekey": "Node:13",
    "Name":"Parada de Tondeia",
    "Latitude":"41.1765780321068",
    "Longitude":"-8.37023578802149",
    "ShortName":"parad",
    "IsDepot":"false",
    "IsReliefPoint":"false"
}
```

##### Criar Linha (US3)

```
POST localhost:8080/lines
```

- Exemplo do Body:

```
{
    "linekey":"Line:5",
    "name":"Sobrosa_Cete",
    "color": "RGB(152,16,147)",
    "linepaths":[
        "5fb16be78aa4fa5040388dfd",
        "5fb16f7aac0e0a4044dd3d8c"
    ]
}
```

##### Definir um Percurso (US4)

```
POST localhost:8080/path
```

- Exemplo do Body:

```
{
    "linepathkey":"LinePath:12",
    "orientation":"GO",
    "path":[
        {
            "pathkey": "Path:22",
            "isempty": "false",
            "pathnodes":[
                {
                    "pathnodekey":"PathNode:91",
                    "node":"5fb164e385eb5c6244a18ceb"
                },
                {
                     "pathnodekey":"PathNode:92",
                     "node":"5faffa4f5fdf0a5ba0d24472",
                     "duration":"240",
                     "distance":"2000"
                }
            ]
        }
    ]
}
```

##### Criar Tipo de Tripulante (US5)

```
POST localhost:8080/drivers
```

- Exemplo do Body:

```
{
    "Code":"2",
    "Description":"Motorista Junior"
}
```

##### Criar Tipo de Viatura (US6)

```
POST localhost:8080/vehicles
```

- Exemplo do Body:

```
{
    "VehicleType":"VehicleType:1",
    "Name":"Autocarro",
    "Autonomy":"500000",
    "Cost":"10",
    "AverageSpeed":"30",
    "EnergySource":"23",
    "Consumption":"30",
    "Emissions": "1050"
}
```
## Heroku

A nossa app encontra-se na cloud Heroku. O seu endereço é: 
https://opt-rse.herokuapp.com/

Nota: Ainda não desenvolvemos a componente UI Users.

---

## Bibliografia

Para além dos conteúdos disponibilizados nas **aulas** e no **Moodle**, também recorremos a outros conteúdos online para auxílio de compreensão e resolução de dúvidas/erros.

[Node.js Tutorial for Begginers: Lear Node in 1 Hour](https://youtu.be/TlB_eWDSMt4),

[Academind - Node Js basics](https://youtu.be/65a5QQ3ZR2g),

[Academind - Build a Node REST API](https://youtu.be/0oXYLzuucwE),

[Mongoose Docs](https://mongoosejs.com/docs/guide.html),

[Mongo DB Compass](https://docs.mongodb.com/compass/master/)

[Email Regex](https://emailregex.com/)

[Academind - Testing Introduction Tutorial](https://youtu.be/r9HdJ8P6GQI)
