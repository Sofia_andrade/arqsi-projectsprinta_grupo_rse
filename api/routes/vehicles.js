var express = require("express");
var router = express.Router();
var checkAuth = require ("../middleware/check-auth")

var vehiclesCTRL = require("../controllers/vehiclesCTRL");

// ROUTE CRIAR NOVO VEHICLE (POST)
router.post("/", checkAuth, vehiclesCTRL.vehicle_post);

// ROUTE LISTAR VEHICLES
router.get("/", checkAuth, vehiclesCTRL.vehicle_get_all);

// ROUTE GET VEHICLE POR ID
router.get("/:_id", checkAuth, vehiclesCTRL.vehicle_get_by_id);

// ROUTE APAGAR VEHICLE
router.delete("/:_id", checkAuth, vehiclesCTRL.vehicle_delete_by_id);

module.exports = router;
