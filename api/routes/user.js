var express = require("express");
var router = express.Router();
var mongoose = require("mongoose");


var usersCTRL = require("../controllers/usersCTRL");

var User = require('../models/userschema');

// ROUTE LISTAR 
router.get("/", usersCTRL.users_get_all);

// ROUTE APAGAR LINE
router.delete("/:_id", usersCTRL.users_delete_by_id);

// SIGNUP
router.post('/signup', usersCTRL.users_signup);

// LOGUIN
router.post('/login', usersCTRL.users_login);


module.exports = router;
