var express = require("express");
var router = express.Router();
var checkAuth = require ("../middleware/check-auth")

var pathCTRL = require("../controllers/pathCTRL");

// CRIAR NOVO PATH (POST)
router.post("/", checkAuth, pathCTRL.path_post);

// LISTAR PERCURSOS
router.get("/", checkAuth, pathCTRL.path_get_all);

// GET POR ID
router.get("/:_id", checkAuth, pathCTRL.path_get_by_id);

// APAGAR
router.delete("/:_id", checkAuth, pathCTRL.path_delete_by_id);

module.exports = router;
