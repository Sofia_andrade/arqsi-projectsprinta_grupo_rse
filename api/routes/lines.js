var express = require("express");
var router = express.Router();
var checkAuth = require ("../middleware/check-auth")

var linesCTRL = require("../controllers/linesCTRL");

// ROUTE CRIAR NOVA LINHA (POST)
router.post("/", checkAuth, linesCTRL.lines_post);

// ROUTE LISTAR LINES
router.get("/", checkAuth, linesCTRL.lines_get_all);

// ROUTE GET LINE POR ID
router.get("/:_id", checkAuth, linesCTRL.lines_get_by_id);

// ROUTE APAGAR LINE
router.delete("/:_id", checkAuth, linesCTRL.lines_delete_by_id);

module.exports = router;
