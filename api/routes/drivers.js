var express = require("express");
var router = express.Router();

var driversCTRL = require("../controllers/driversCTRL");
var checkAuth = require("../middleware/check-auth");

// CRIAR NOVO DRIVER
router.post("/", checkAuth, driversCTRL.drivers_post);

// ROUTE LISTAR DRIVERS
router.get("/", checkAuth, driversCTRL.drivers_get_all);

// ROUTE GET DRIVER POR ID
router.get("/:_id", checkAuth, driversCTRL.drivers_get_by_id);

// APAGAR DRIVER POR ID
router.delete("/:_id", checkAuth, driversCTRL.drivers_delete_by_id);

module.exports = router;
