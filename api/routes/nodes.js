var express = require("express");
var router = express.Router();
var checkAuth = require ("../middleware/check-auth")

var nodesCTRL = require("../controllers/nodesCTRL");

// CRIAR NOVO Nó (POST)
router.post("/", checkAuth, nodesCTRL.node_post);

// LISTAR NóS
router.get("/", checkAuth, nodesCTRL.node_get_all);

// GET POR ID
router.get("/:_id", checkAuth, nodesCTRL.node_get_by_id);

// APAGAR
router.delete("/:_id", checkAuth, nodesCTRL.node_delete_by_id);

module.exports = router;
