const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
var app = express();

// UPLOAD de ficheiros com multer - WORKING
app.use(express.static("public"));
const FileUpload = require("./public/index");
app.use("/uploads", FileUpload);

//ligação ao Mongo DB - WORKING
mongoose.connect(
  "mongodb+srv://sofia_and:ArqSi@cluster0.lxwrl.mongodb.net/<dbname>?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

//disponibilizaçao do módulo body parser.
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//Rotas
const userRoutes = require("./routes/user");
app.use("/user", userRoutes);
const nodesRoutes = require("./routes/nodes");
app.use("/nodes", nodesRoutes);
const vehiclesRoutes = require("./routes/vehicles");
app.use("/vehicles", vehiclesRoutes);
const LinesRoutes = require("./routes/lines");
app.use("/lines", LinesRoutes);
const pathRoutes = require("./routes/path");
app.use("/path", pathRoutes);
const driversRoutes = require("./routes/drivers");
app.use("/drivers", driversRoutes);

//
app.use(function (req, res) {
  res.status(200).json({
    message: "Funciona",
  });
});

//set up de erros

app.use(function (req, res, next) {
  const error = new Error("Not found");
  error.status = 404;
  next(error);
});

app.use(function (error, req, res, next) {
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message,
    },
  });
});

module.exports = app;
