var express = require("express");
var router = express.Router();

//UPLOAD DE FICHEIROS - WORKING
const multer = require("multer");
const { response } = require("../app");
const app = require("../app");
const { listIndexes } = require("../models/nodeschema");

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./api/uploads/");
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  },
});

const upload = multer({ storage: storage });

router.post("/", upload.single("file"), (req, res) => {
  console.log(req.file);
  res.json({ message: "Ficheiro Importado" });
});

module.exports = router;

//CONVERSÃO DE XML PARA JSON - NOT FINISHED

const fs = require("fs");
const xml2js = require("xml2js");
