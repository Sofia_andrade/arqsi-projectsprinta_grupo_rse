var jwt = require("jsonwebtoken");

// VERIFICA A EXISTENCIA DE UM TOKEN VALIDO
function verifyToken(req, res, next) {
  var token = req.headers["x-access-token"];
  if (!token)
    return res.status(403).send({ auth: false, message: "No token provided" });
  jwt.verify(token, "secret", function (err, decoded) {
    if (err)
      return res
        .status(500)
        .send({ auth: false, message: "Failed to authenticate token." });
    req.user = decoded.user;
    next();
  });
}
module.exports = verifyToken;

//FALTA A CRIAÇÃO DE ROLES - ADMIN/CLIENT/GESTOR
