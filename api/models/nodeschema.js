var mongoose = require("mongoose");

var Schema = mongoose.Schema;

//NOTE: criação schema auxiliar Crew Travel time
//TO FIX: validações

var CrewTravelTimeSchema = new Schema({
  Crewtraveltimekey: {
    type: String,
  },
  Duration: {
    type: Number,
  },
  Node: {
    type: String,
  },
});

// NOTE: criaçao do objeto schema para Nós
// TO FIX:falta validação DOS properties, EX. LENGHT, CODIGO WGS84,

var nodesSchema = new Schema({
  Nodekey: {
    type: String,
    required: true,
    unique: true,
  },
  Name: {
    type: String,
    required: true,
  },
  Latitude: {
    type: String,
    required: true,
  },
  Longitude: {
    type: String,
    required: true,
  },
  ShortName: {
    type: String,
    required: true,
    unique: true,
    uppercase: true,
  },
  IsDepot: {
    type: Boolean,
  },
  IsRelieafPoint: {
    type: Boolean,
  },
  CrewTravelTime: {
    type: CrewTravelTimeSchema,
  },
});

module.exports = mongoose.model("Node", nodesSchema);
