var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var vehicleSchema = new Schema({
  VehicleType: {
    type: String,
    required: true,
    unique: true
  },
  Name: {
    type: String,
    required: true
  },
  Autonomy: {
    type: Number,
    required: true
  },
  Cost: {
    type: Number,
    required: true
  },
  AverageSpeed: {
    type: Number,
    required: true
  },
  EnergySource: {
    type: Number,
    required: true
  },
  Consumption: {
    type: Number,
    required: true
  },
  Emissions: {
    type: Number,
    required: false
  }
});

module.exports = mongoose.model("Vehicles", vehicleSchema);
