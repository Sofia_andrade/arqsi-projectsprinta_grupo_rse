var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var driverSchema = new Schema({
  Code: { type: Number, 
    required: true},
  Description: { type: String }
});

module.exports = mongoose.model("Drivers", driverSchema);