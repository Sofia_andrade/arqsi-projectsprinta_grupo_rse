var mongoose = require("mongoose");
const linepathsSchema = require("./pathschema");

var Schema = mongoose.Schema;


//NOTE: criação Schema para LINHAS
//TO FIX: definição correta de properties (ex. name como composição dos nós iniciais e finais e a color como RBG code)
//TO FIX: allowed/denied drivers e vehicles

var lineSchema = new Schema({
  linekey: {
    type: String,
    required: true,
    unique: true,
  },
  name: { type: String },
  color: { type: String },
  linepaths: [{ type: Schema.Types.ObjectId, ref: "Path" }],
  allowedVehicles: { type: String },
  deniedVehicles: { type: String },
  allowedDrivers: { type: String },
  deniedDrivers: { type: String },
});

module.exports = mongoose.model("Line", lineSchema);
