var mongoose = require("mongoose");
const nodeschema = require("./nodeschema");

var Schema = mongoose.Schema;

//NOTE: criação Schema auxiliar: Segmento de Nó
//TO FIX: por apurar dúvida de negócio na implementação: lógica sequencial A->B->C->D OU [A,B]->[B,C]->[C,D]. Neste momento está a primeira.
//TO FIX: em falta validações das properties, como lenght

var pathnodeSchema = new Schema({
  pathnodekey: {
    type: String,
    required: true,
    unique: true,
  },
  node: {
    type: Schema.Types.ObjectId, ref: "Node"
  },
  duration: {
    type: Number,
  },
  distance: {
    type: Number,
  },
});

//NOTE:criação schema para Percurso
//
var pathSchema = new Schema({
  pathkey: {
    type: String,
    required: true,
    unique: true,
  },
  isempty: {
    type: Boolean,
  },
  pathnodes: [pathnodeSchema],
});


//NOTE: criação schema auxiliar linepath
//TO FIX: linepathkey deve ser required e unique? supostamente sim (todas as linhas tem pelo menos 1 percurso)
//TO FIX: orientation só deveria permitir 2 tipos de valores (Go/Return)

var linepathsSchema = new Schema({
  linepathkey: {
    type: String,
    required: true,
    unique: true,
  },
  path: [{
    type: pathSchema
  }],
  orientation: {
    type: String,
    uppercase: true
  },
});
module.exports = mongoose.model("Path", linepathsSchema);
