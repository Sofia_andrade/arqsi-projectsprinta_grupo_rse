var bcrypt = require("bcrypt");
var jwt = require('jsonwebtoken');

var User = require("../models/userschema.js");

// LISTAR UTILIZADORES
exports.users_get_all = function (req, res) {
  User.find(function (err, user) {
    if (err) res.send(err);
    res.json(user);
  });
};

// DELETE PERCURSO POR ID
exports.users_delete_by_id = function (req, res, next) {
  const id = req.params._id;
  User.findOneAndDelete({ _id: id })
    .exec()
    .then((result) => {
      res.status(200).json({result});
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        error: err,
      });
    });
};

exports.users_signup = function (req, res) {
	User.find({ email: req.body.email })
		.exec()
		.then(user => {
			if (user.length >= 1) {
				return res.status(409).json({
					message: 'email exists'
				});
			} else {
				bcrypt.hash(req.body.password, 10, (err, hash) => {
					if (err) {
						return res.status(500).json({
							error: err
						});
					} else {
						var user = new User({
							email: req.body.email,
							password: hash
						});
						user
							.save()
							.then(result => {
								console.log(result);
								res.status(201).json({
									message: 'User created'
								});
							})
							.catch(err => {
								console.log(err);
								res.status(500).json({
									error: err
								});
							});
					};
				});
			}
		})
};

exports.users_login = function (req, res) {
	User.find({ email: req.body.email})
	.exec()
	.then(user => {
		if (user.length < 1) {
			return res.status(401).json({
				message: "Auth failed"
			});
		}
		bcrypt.compare(req.body.password, user[0].password, (err, result) => {
			if (err) {
				return res.status(401).json({
					message: "Auth failed"
				});
			}
			if (result) {
				const token = jwt.sign({
					email: user[0].email,
					userId: user[0]._id
				}, 
				"secret",
				{
					expiresIn: "1h"
				}
				);
				return res.status(200).json({
					message: 'Auth successful',
					token: token
				});
			}
			return res.status(401).json({
				message: "Auth failed"
			});
		});
	})
	.catch(err => {
		console.log(err);
		res.status(500).json({
			error: err
		});
	});
};

