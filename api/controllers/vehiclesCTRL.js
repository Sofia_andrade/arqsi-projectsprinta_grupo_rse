var Vehicles = require("../models/vehicleschema.js");

// LISTAR VEHICLES
exports.vehicle_get_all = function (req, res) {
  Vehicles.find(function (err, vehicles) {
    if (err) res.send(err);
    res.json(vehicles);
  });
};

// LISTAR VEHICLE POR ID
exports.vehicle_get_by_id = function (req, res, next) {
  const id = req.params._id;
  Vehicles.findById(id)
    .exec()
    .then((doc) => {
      console.log("From database", doc);
      if (doc) {
        res.status(200).json(doc);
      } else {
        res.status(404).json({
          message: "No valid entry found for ID"
        });
      }
    });
};

// DELETE VEHICLE POR ID
exports.vehicle_delete_by_id = function (req, res, next) {
  const id = req.params._id;
  Vehicles.findOneAndDelete({ _id: id })
    .exec()
    .then((result) => {
      res.status(200).json(result);
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

// CRIAR VEHICLE
// TO FIX: http response deveria ser 201
exports.vehicle_post = function (req, res) {
  var vehicle = new Vehicles();
  vehicle.VehicleType = req.body.VehicleType;
  vehicle.Name = req.body.Name;
  vehicle.Autonomy = req.body.Autonomy;
  vehicle.Cost = req.body.Cost;
  vehicle.AverageSpeed = req.body.AverageSpeed;
  vehicle.EnergySource = req.body.EnergySource;
  vehicle.Consumption = req.body.Consumption;
  vehicle.Emissions = req.body.Emissions;
  vehicle.save(function (err) {
    if (err) res.send(err)
    else
    res.json({ message: "Vehicle created!", vehicle });
  });
};
