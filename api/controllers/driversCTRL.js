var Drivers = require("../models/driverschema.js");


// LISTAR DRIVERS
exports.drivers_get_all = function (req, res) {
  Drivers.find(function (err, drivers) {
    if (err) res.send({message: "não da"}, err);
    else
    res.json({message: "acedido", drivers});
  });
};

// LISTAR DRIVER POR ID
exports.drivers_get_by_id = function (req, res, next) {
  const id = req.params._id;
  Drivers.findById(id)
    .exec()
    .then((doc) => {
      console.log("From database", doc);
      if (doc) {
        res.status(200).json(doc);
      } else {
        res.status(404).json({
          message: "No valid entry found for ID"
        });
      }
    });
};

// DELETE DRIVER POR ID
exports.drivers_delete_by_id = function (req, res, next) {
  const id = req.params._id;
  Drivers.findOneAndDelete({ _id: id })
    .exec()
    .then((result) => {
      res.status(200).json(result);
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

// CRIAR DRIVERS
// TO FIX: http response deveria ser 201
exports.drivers_post = function (req, res) {
  var drivers = new Drivers();
  drivers.Code = req.body.Code;
  drivers.Description = req.body.Description;
  drivers.save(function (err) {
    if (err) res.send(err)
    else
    res.json({ message: "Driver created!" });
  });
};