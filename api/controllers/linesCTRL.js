var Line = require("../models/lineschema.js");

// LISTAR LINES
exports.lines_get_all = function (req, res) {
  Line.find(function (err, lines) {
    if (err) res.send(err);
    res.json(lines);
  });
};

// LISTAR LINE POR ID
exports.lines_get_by_id = function (req, res, next) {
  const id = req.params._id;
  Line.findById(id)
    .exec()
    .then((doc) => {
      console.log("From database", doc);
      if (doc) {
        res.status(200).json(doc);
      } else {
        res.status(404).json({
          message: "No valid entry found for ID"
        });
      }
    });
};

// DELETE LINE POR ID
exports.lines_delete_by_id = function (req, res, next) {
  const id = req.params._id;
  Line.findOneAndDelete({ _id: id })
    .exec()
    .then((result) => {
      res.status(200).json(result);
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

// CRIAR LINE
// TO FIX: http response deveria ser 201
exports.lines_post =  function (req, res) {
  var line = new Line();
  line.linekey = req.body.linekey;
  line.name = req.body.name;
  line.color = req.body.color;
  line.linepaths = req.body.linepaths;
  line.allowedVehicles = req.body.alloewedVehicles;
  line.deniedVehicles = req.body.deniedVehicles;
  line.allowedDrivers = req.body.alloewedDrivers;
  line.deniedDrivers = req.body.deniedVehicles;

  line.save(function (err) {
    if (err) res.send(err)
    else
    res.json({ message: "Line created!", line });
  });
};
