var Path = require("../models/pathschema.js");

// LISTAR PERCURSOS
exports.path_get_all = function (req, res) {
  Path.find(function (err, path) {
    if (err) res.send(err);
    res.json(path);
  });
};

// LISTAR PERCURSO POR ID
exports.path_get_by_id = function (req, res, next) {
  const id = req.params._id;
  Path.findById(id)
    .exec()
    .then((doc) => {
      console.log("From database", doc);
      if (doc) {
        res.status(200).json(doc);
      } else {
        res.status(404).json({
          message: "No valid entry found for ID",
        });
      }
    });
};

// DELETE PERCURSO POR ID
exports.path_delete_by_id = function (req, res, next) {
  const id = req.params._id;
  Path.findOneAndDelete({ _id: id })
    .exec()
    .then((result) => {
      res.status(200).json({ message: "Path Deleted:" }, result);
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        error: err,
      });
    });
};

// CRIAR PERCURSO
// TO FIX: http response deveria ser 201
exports.path_post = function (req, res) {
  var path = new Path();
  path.linepathkey = req.body.linepathkey;
  path.path = req.body.path;
  path.orientation = req.body.orientation;
  path.save(function (err) {
    if (err) res.send(err)
    else
    res.json({ message: "LinePath created:", path });
  });
};
