var Node = require("../models/nodeschema.js");

// LISTAR NOS
exports.node_get_all = function (req, res) {
  Node.find(function (err, nodes) {
    if (err) res.send(err);
    res.json(nodes);
  });
};

// LISTAR NO POR ID
exports.node_get_by_id = function (req, res, next) {
  const id = req.params._id;
  Node.findById(id)
    .exec()
    .then((doc) => {
      console.log("From database", doc);
      if (doc) {
        res.status(200).json(doc);
      } else {
        res.status(404).json({
          message: "No valid entry found for ID",
        });
      }
    });
};

// DELETE NO POR ID
exports.node_delete_by_id = function (req, res, next) {
  const id = req.params._id;
  Node.findOneAndDelete({ _id: id })
    .exec()
    .then((result) => {
      res.status(200).json({ message: "Node Deleted:", result });
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        error: err,
      });
    });
};

// CRIAR NO
// TO FIX: http response deveria ser 201
exports.node_post = function (req, res) {
  var node = new Node();
  node.Nodekey = req.body.Nodekey;
  node.Name = req.body.Name;
  node.Latitude = req.body.Latitude;
  node.Longitude = req.body.Longitude;
  node.ShortName = req.body.ShortName;
  node.IsDepot = req.body.IsDepot;
  node.IsRelieafPoint = req.body.IsRelieafPoint;
  node.CrewTravelTime = req.body.CrewTravelTime;
  node.save(function (err) {
    if (err) res.send(err)
    else
    res.json({message: "Node Created:", node})
  });
};
