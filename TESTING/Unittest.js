const { TestScheduler } = require("jest");
var Drivers = require("./api/models/driverschema.js");

exports.Drivers = (code, description) => {
  return `${code} AND ${description}.`;
};
