const { Drivers } = require("./Unittest.js");

test("Should output code and description of a Driver", () => {
  const text = Drivers(123, 123);
  expect(text).toBe("123 AND 123.");
  const text2 = Drivers("aaa", "bbb");
  expect(text2).toBe("aaa AND bbb.");
});

test("Should output data less text", () => {
  const text = Drivers("", "");
  expect(text).toBe(" AND .");
});
